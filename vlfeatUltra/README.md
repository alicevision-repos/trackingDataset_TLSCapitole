# OpenMVG reconstruction

This is the 3D reconstruction obtained with the Incremental pipeline of OpenMVG. 
For this reconstruction the SIFT features have been used (VLFeat implementation) 
with a ULTRA setting in order to maximize the point density. In order to see all 
the parameter values used for the feature extraction please check the [image_describer.json](matches/image_describer.json).

The folder `matches` contains the extracted features and descriptors and the file containing
the matches between images.

The folder `reconstruction` contains the result of the Incremental reconstruction.
In particular [sfm_data.bin](matches/sfm_data.bin) contains all the 3D points,
the camera poses and intrinsics for the reconstructed scene.

Please refer to OpenMVG documentation for more informations.


## References

- OpenMVG Library: https://github.com/openMVG/openMVG
- OpenMVG documentation: http://openmvg.readthedocs.org/en/latest/