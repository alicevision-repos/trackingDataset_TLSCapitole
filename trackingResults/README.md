# Tracking results

Here you can find the tracking results for the videos provided in the `videos` folder and the data necessary to replicate the experiments. The results are in Alembic format (`abc`), an open computer graphics interchange format that contains the 3D point cloud from the reconstruction and the animated camera with the estimated poses.

### Results
For each video, 2 abc files are provided. The first one contains the raw output of the localization for each frame of the video, while the one with `BUNDLE` contains the same result after a global bundle adjustment over all the frames.

### Visualizing the results in Maya
In order to visualize the results, you can import them into Maya with
```
Pipeline Cache -> Alembic Cache -> Open Alembic
```
The you you can add the image plane to the camera: from the attribute panel, select `Environment` and `Create`. Then select one of the images of the sequence from the `Image name` combo box, and then check the `Use image sequence` check box.

## Vocabulary tree

The vocabulary tree is used for localization. Here you can find two files containing the tree and its weights, `voctreeK10L5VLFEATfloat.tree` and `voctreeK10L5VLFEATfloat.weights` respectively. You can generate your own vocabulary tree using the executable in POPART fork of OpenMVG `openMVG_main_createVoctree`.

## Python script

`scriptCapitoleULTRA.py` is a python script wrapping the `openMVG_main_voctreeLocalizer` executable. Beside having an exact mapping of the input parameter of the executable, it allows to call the localization with some parameter by default and with a minimal interface.

In order to use the script you have to first edit the following 2 global variables at the beginning of the file:

```python
# the full path of the executable to call
executable = '/usr/local/openMVG/openMVG_main_voctreeLocalizer'

# the directory containing this dataset
BASEPATH='/home/simone/dev/code/lib/data/ImageDataset_CapitoleTLS'

# DO NOT CHANGE - the directory containing the OpenMVG reconstruction)
MVGPATH=BASEPATH+'/vlfeatUltra'
# DO NOT CHANGE - the directory containing the shots)
TLSSHOT=BASEPATH+'videos'
```

Then the most simple way to call the script is

```bash
scriptCapitoleULTRA.py shotNumber [mediafile]
```

where `shotNumber` is the ID of the shot (eg `00039`) and the optional `mediafile` 
can be the media to localize (an image, a .txt, a json, a video) as the parameter 
`--mediafile` of `openMVG_main_voctreeLocalizer`. If provided, the media is 
supposed to be located in `shotNumber` directory w.r.t. `TLSSHOT` variable. If not 
provided, it will assume that a directory named `shotNumber` exist containing the 
images to localize. So for example calling

```
scriptCapitoleULTRA.py 00039 
```

it will localize all the images contained in `./videos/00039`. 

```
scriptCapitoleULTRA.py 00039 img.0001.png
```

will localize the image img.0001.png contained in `./videos/00039`.

The script does not support video as input: for that you have to use the original
executable and specify the full path to the video file with the `--mediafile` parameter.


## References

- Localization executable on POPART OpenMVG fork: https://github.com/poparteu/openMVG/blob/popart_opengv/src/software/Localization/main_voctreeLocalizer.cpp

- Vocabulary tree creation on POPART OpenMVG fork: https://github.com/poparteu/openMVG/blob/popart_opengv/src/software/Localization/createVoctree.cpps