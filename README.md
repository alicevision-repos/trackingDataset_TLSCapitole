# Toulouse Capitole camera tracking dataset

![](tlsdatasettracking.png)

## The dataset
This dataset collects 56 images taken in [Place du Capitole](https://goo.gl/maps/rWyj8yCGAMP2) in Toulouse (FRA) and 4 related videos taken while moving around in the place. The aim of the dataset is to test and evaluate the camera tracking algorithms developed for the [POPART](http://www.popartproject.eu/) project, and help the reproducibility of the experiments.

The camera tracking algorithms developed for the POPART project are based on model tracking of an existing 3D reconstruction of the scene. First a collection of still images are taken and a SfM pipeline is used to perform the 3D reconstruction of the scene. As result of this first step, a 3D point cloud is generated. The camera tracking algorithms are then based on camera localization techniques: each frame is individually localized w.r.t. the 3D point cloud using the photometric information (SIFT features) associated to each point. This allows to align the point cloud to the current frame and thus compute the camera pose.

Videos showing the camera tracking results for the videos contained in this dataset are available here:

 - Capitole 00040: https://youtu.be/zU_aJtA61Zs
 - Capitole 00040: https://youtu.be/I6oltk9iVUU
 - Capitole 00041: https://youtu.be/X7zKK-W-qRQ
 - Capitole 00042: https://youtu.be/zU_aJtA61Zs



## Dataset Structure
The dataset is composed of the following directories:

 - `images` contains the 56 original images;
 - `vlfeatULTRA` contains the data and the result of the incremental SfM pipeline of OpenMVG used for tracking the camera in the videos;
 - `videos` contains the 4 videos that has been used to test the tracking algorithms along with their calibration files
 - `trackingResults` contains the tracking results for the 4 videos in Alembic format (abc), which can be imported into Maya for visualization, and the data for replicating them. 

Check the different `README.md` inside each directory for further information on how the data has been generated or how to reproduce the results.

## License

This dataset is released under the Creative Commons Attribution-ShareAlike 3.0 Unported license (see [LICENSE.md](LICENSE.md))

## References

 - The POPART project: http://www.popartproject.eu
 - OpenMVG library (POPART fork): https://github.com/alicevision/openMVG 
 - Alembic open computer graphics interchange framework: http://www.alembic.io/
 - Maya® 3D animation, modeling, simulation, and rendering software: http://www.autodesk.com/products/maya/overview
 
## Citation

If you are referring to or using this dataset please cite it as 

```
Gasparini, S. (2016). Toulouse Capitole camera tracking dataset. Zenodo. doi:10.5281/zenodo.49896
```

BibTex
```
@misc{Gasparini2016Toulouse,
    author = {Gasparini, Simone},
    title  = {{Toulouse Capitole camera tracking dataset}},
    publisher = {Zenodo},
    doi    = {10.5281/zenodo.49896},
    url    = {http://dx.doi.org/10.5281/zenodo.49896},
    month  = apr,
    year   = {2016}
}
```

## Acknowledgements
This dataset has been developed during the [POPART](http://www.popartproject.eu/) project funded by the European Union’s Horizon 2020 research and innovation programme under grant agreement No 644874.

## Author

Simone Gasparini (simone.gasparini@gmail.com)